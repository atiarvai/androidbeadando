package com.beres_tibor.androidbeadando;

import com.google.gson.annotations.SerializedName;

public class Post {

    private int userid;
    private int id;
    private  String title;

    public int getUserid() {
        return userid;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    @SerializedName("body")
    private String text;
}
